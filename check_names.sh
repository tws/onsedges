#!/bin/bash

for i in $(cut -f3 -d, nhic_sedges.csv | rev | cut -d' ' -f1 | rev)
do
    grep -o -q $i ./*.tex
    if [ $? -eq 1 ]
    then
        # Print out the missing values
        grep $i nhic_sedges.csv | cut -d, -f3
    fi
done
